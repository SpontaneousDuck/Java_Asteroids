package asteroids;

import javafx.geometry.Point2D;
import javafx.scene.image.Image;

public class Shot extends Sprite{

    private static final double velocityX = 3, velocityY = 3;
    public static double lastShotTime;
    public long shotTimer = 0;// used to make a shot disappear after a specified amount of time determined in Main

    Shot(double x,double y, double a){
        super();
        setImage(new Image("shot.png"), 7, 50);
        setCenterPosition(x,y);
        setAngle(a);
        addVelocityAtAngle(400);
    }

    Shot(Point2D p, double a, long timer){
        this(p.getX(), p.getY(), a);
        this.shotTimer = timer;
    }

    Shot(Spaceship ship){
        this(ship.center.getX(),ship.center.getY(), ship.angle);
    }
    public Sprite ifHit(Sprite s, double time){
        if (intersects(s)){
            //erase shot
            return s;
        }
        return null;
    }


    public boolean hitBorder(double maxRight, double maxUp, double maxLeft, double maxDown){
        return (this.positionX >= maxRight) || (this.positionY >= maxUp) || (this.positionX <= maxLeft) || (this.positionY <= maxDown);
    }
}