package asteroids;

import javafx.geometry.Point2D;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.image.Image;

public class Spaceship extends Sprite{
	//private double accel;	//add in acceleration if possible

	public static int lifeCounter = 6;
	public static final int RESPAWN_DELAY = 1000;	//1 second -- used in main
	private static final double SHOT_DELAY = 300000000;	//0.3 seconds
	public static long respawnTimer = 0;



	public Spaceship(){
		this(0,0);
	}

	public Spaceship(double x,double y){	//used to restart the ship wherever it last died
		super();
		setImage(new Image("ship.png"), 50, 50);	//give a filepath to image=
		this.setPosition(x,y);
		System.out.printf("New Spaceship %s created!%n", this.toString());
	}

	public Spaceship(Point2D p, double angle){
		this(p.getX(),p.getY());
		this.angle = angle;
	}

	public static void loseLife(){
		if (canRespawn()){
			lifeCounter--;
		}
	}

	public static boolean canRespawn(){
		if (lifeCounter > 0){
			return true;
		}
		return false;
	}

	public boolean isDead(Sprite s){
		if(intersects(s) && (s instanceof Asteroid)){
			//erase spaceship
			if (canRespawn()){
				lifeCounter--;
				//wait RESPAWN_DELAY
				loseLife();
			}
			else{
				return true;
			}
		}
		return false;
	}
	public Shot fire(double time) {
		if (SHOT_DELAY <= time - Shot.lastShotTime) {
			Shot.lastShotTime = time;
			return new Shot(this);
		}
		return null;
	}


}
