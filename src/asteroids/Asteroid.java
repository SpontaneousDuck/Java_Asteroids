package asteroids;

import java.util.ArrayList;
import java.util.ListIterator;
import java.util.Vector;

import javafx.scene.canvas.GraphicsContext;
import javafx.scene.image.Image;

public class Asteroid extends Sprite {
    private static int num = 0;
	private int hits;
    private int sw;
    private int sh;

    /**
     * @param x width of screen
     * @param y height of screen
     */
	public Asteroid(int x, int y, int s)
	{
		num += 1;
		image = new Image("a.png");
		sw = x;
		sh = y;
		//Small asteroid
		if (s == 1)
		{
			height = 40;
			width = 40;
			hits = 1;
		}
		//Medium Asteroid
		if (s == 2)
		{
			height = 80;
			width = 80;
			hits = 2;
		}
		//Large asteroid
		if (s == 3)
		{
			height = 120;
			width = 120;
			hits = 3;
		}
		chooseSide();
	}

	/**
	 *
	 * @return num number of Asteroids
	 */
	public static int getNum()
	{
		return num;
	}

	/**
	 * subtracts total number of asteroids by 1
	 */
	public static void remove()
	{
		num -= 1;
	}

	/**
	 * method for when an asteroid is hit
	 * lives -1
	 * height and width subtracted by 40
	 * @return amount of lives left
	 */
	public void updateHits()
	{
		hits -= 1;
		width -= 40;
		height -= 40;
	}

	public int getHits()
	{
		return hits;
	}

	public Sprite isHit(Vector<Sprite> objects)
	{
		for (Sprite e : objects) {
			if (e instanceof Shot) {
				if (intersects(e)) {
					updateHits();
					velocityX *= 1.5;
					velocityY *= 1.5;
					return e;
				}
			}
			if (e instanceof Spaceship)
			{
				Spaceship a = (Spaceship) e;
				if (intersects(a))
				{
					a.respawn();
					return a;
				}
			}
		}
		return null;
	}

	public void update(double time)
    {
		positionX += velocityX * time;
		positionY += velocityY * time;
		//left side
		if ((positionX + width) < 0)
			positionX = sw;
		//right side
		if ((positionX - width) > sw)
			positionX = 0;
		//top side
		if ((positionY + height) < 0)
			positionY = sh;
		//bottom side
		if ((positionY - height) > sh)
			positionY = 0;

		center = getCenter();
	}

	/**
	 * Randomizes the side the asteroid starts on and
	 * give it a random angle(velocity) to move at
	 */
	private void chooseSide()
	{
		int side = (int)(Math.random() * 4) + 1;
		//top side
		if (side == 1)
		{
			positionX = (Math.random() * sw) + 1;
			positionY = 0;
			velocityX = (int)(Math.random() * 100) - 50;
			velocityY = (int)(Math.random() * 50) + 1;
		}
		//right side
		if (side == 2)
		{
			positionX = sw;
			positionY = (Math.random() * sh) + 1;
			velocityX = (int)(Math.random() * 49) - 50;
			velocityY = (int)(Math.random() * 100) - 50;
		}
		//bottom side
		if (side == 3)
		{
			positionX = (Math.random() * sw) + 1;
			positionY = sh;
			velocityX = (int)(Math.random() * 100) - 50;
			velocityY = (int)(Math.random() * 49) - 50;
		}
		//left side
		if (side == 4)
		{
			positionX = 0;
			positionY = (Math.random() * sh) + 1;
			velocityX = (int)(Math.random() * 50) + 1;
			velocityY = (int)(Math.random() * 10) - 50;
		}
	}

}