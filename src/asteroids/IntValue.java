package asteroids;

/**
 * Created by withamk on 3/16/2017.
 */
public class IntValue {
    public int value;

    public IntValue(int val) {
        value = val;
    }

    public String toString() {
        return Integer.toString(value);
    }
}