package asteroids;
import javafx.geometry.Point2D;
import javafx.scene.image.Image;
import javafx.scene.canvas.GraphicsContext;
import javafx.geometry.Rectangle2D;
import javafx.scene.transform.Rotate;

public abstract class Sprite
{
    protected Image image;
    protected double positionX;
    protected double positionY;
    protected double velocityX;
    protected double velocityY;
    protected double width;
    protected double height;
    protected double angle;
    protected Point2D center, position;


    public Sprite()
    {
        positionX = 0;
        positionY = 0;
        velocityX = 0;
        velocityY = 0;
        angle = 0;
    }

    public void setImage(Image i)
    {
        image = i;
        width = i.getWidth();
        height = i.getHeight();
        center = getCenter();
    }

    void setImage(Image i, double w, double h)
    {
        image = i;
        width = w;
        height = h;
        center = getCenter();
    }

    public void setImage(String filename)
    {
        Image i = new Image(filename);
        setImage(i);
    }

    public void setPosition(double x, double y)
    {
        positionX = x;
        positionY = y;
        position = new Point2D(x,y);
    }

    public void setPosition(Point2D p)
    {
        positionX = p.getX();
        positionY = p.getY();
        position = p;
    }

    public void setCenterPosition(double x, double y) {
        positionX = x - width/2;
        positionY = y - height/2;
    }

    public void setVelocity(double x, double y)
    {
        velocityX = x;
        velocityY = y;
    }

    public void setAngle(double a) {
        angle = a%360;
    }

    public void addVelocity(double x, double y)
    {
        velocityX += x;
        velocityY += y;
    }

    public void addVelocityAtAngle(double amtToAdd){
    	double rad = Math.toRadians(this.angle);
    	addVelocity(amtToAdd * Math.sin(rad), -amtToAdd * Math.cos(rad));
    }
    public void update(double time)
    {
        positionX += velocityX * time;
        positionY += velocityY * time;
        center = getCenter();
    }

    public void render(GraphicsContext gc)
    {
        gc.save(); // saves the current state on stack, including the current transform
        rotate(gc, angle, center.getX(), center.getY());
        gc.drawImage( image, positionX, positionY, width, height);
        gc.restore(); // back to original state (before rotation)
    }

    /**
     * Sets the transform for the GraphicsContext to rotate around a pivot point.
     *
     * Source: http://stackoverflow.com/questions/18260421/how-to-draw-image-rotated-on-javafx-canvas
     *
     * @param gc the graphics context the transform to applied to.
     * @param angle the angle of rotation.
     * @param px the x pivot co-ordinate for the rotation (in canvas co-ordinates).
     * @param py the y pivot co-ordinate for the rotation (in canvas co-ordinates).
     */
    protected void rotate(GraphicsContext gc, double angle, double px, double py) {
        Rotate r = new Rotate(angle, px, py);
        gc.setTransform(r.getMxx(), r.getMyx(), r.getMxy(), r.getMyy(), r.getTx(), r.getTy());
    }

    public Rectangle2D getBoundary()
    {
        return new Rectangle2D(positionX,positionY,width,height);
    }

    protected Point2D getCenter() {
        return new Point2D((positionX+width/2), (positionY+height/2));
    }

    public boolean intersects(Sprite s)
    {
        return s.getBoundary().intersects( this.getBoundary() );
    }

    public String toString()
    {
        return " Position: [" + positionX + "," + positionY + "]"
                + " Velocity: [" + velocityX + "," + velocityY + "]"
                + "Rotation: " + angle;
    }

    public Point2D flipOverBorder(double maxX, double maxY, double minX, double minY){
        if ((maxX <= minX)||(maxY <= minY)) return null;
        if (this.positionX >= maxX) return new Point2D(minX + 1, this.positionY);
        if (this.positionY >= maxY) return new Point2D(this.positionX, minY + 1);
        if (this.positionX <= minX) return new Point2D(maxX - 1, this.positionY);
        if (this.positionY <= minY) return new Point2D(this.positionX, maxY - 1);
        return null;
    }

    public boolean hitBorder(double maxRight, double maxUp, double maxLeft, double maxDown){
        return (this.positionX >= maxRight) || (this.positionY >= maxUp) || (this.positionX <= maxLeft) || (this.positionY <= maxDown);
    }
}