package asteroids;

import javafx.animation.AnimationTimer;
import javafx.application.Application;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.control.TextField;
import javafx.scene.layout.Border;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.stage.Stage;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.ListIterator;
import java.util.Vector;

public class Main extends Application {

    private static final int PLAYABLE_X = 800;
    private static final int PLAYABLE_Y = 800;
    @Override
    public void start(Stage primaryStage) throws Exception{
        IntValue score = new IntValue(0);
        primaryStage.setTitle(String.format("Asteroids! Score: %s", score.toString()));


        Group root = new Group();
        Scene theScene = new Scene( root );
        primaryStage.setScene( theScene );
        TextField tfLife = new TextField();
        tfLife.setBorder(Border.EMPTY);
        tfLife.relocate(0,PLAYABLE_Y);
        tfLife.setDisable(true);
        tfLife.setFont(Font.font("Comic Sans MS", FontWeight.BOLD,20));

        Canvas canvas = new Canvas( PLAYABLE_X, PLAYABLE_Y );
        root.getChildren().addAll( canvas,tfLife );

        Vector<String> keyInput = new Vector<>();

        theScene.setOnKeyPressed(
                e -> {
                    String code = e.getCode().toString();
                    if ( !keyInput.contains(code) )
                        keyInput.add( code );
                });

        theScene.setOnKeyReleased(
                e -> {
                    String code = e.getCode().toString();
                    keyInput.remove( code );
                });

        GraphicsContext gc = canvas.getGraphicsContext2D();

        Vector<Sprite> objects = new Vector<>();
        Vector<Sprite> toRemove = new Vector<>();

        Sprite y[] = new Asteroid[10];
        for (int i = 0; i < 10; i++)
        {
            int e = (int)(Math.random() * 3) + 1;
            y[i] = new Asteroid(PLAYABLE_X, PLAYABLE_Y, e);
            objects.add(y[i]);
        }

        Spaceship ship = new Spaceship((PLAYABLE_X / 2), (PLAYABLE_Y / 2));
        objects.add(ship);

        LongValue lastNanoTime = new LongValue(System.nanoTime());

        //runs every frame
        new AnimationTimer()
        {
            public void handle(long currentNanoTime)
            {


                // calculate time since last update.
                double elapsedTime = (currentNanoTime - lastNanoTime.value) / 1000000000.0;
                lastNanoTime.value = currentNanoTime;
                tfLife.setText(String.format("Lives: %s",Spaceship.lifeCounter-1));

                //exit game if all lives lost
                if (Spaceship.lifeCounter <= 0){
                    System.exit(0);
                }

                //clear screen to draw again
                gc.clearRect(0, 0, PLAYABLE_X,PLAYABLE_Y);

                //iterate over object array and execute code on it (game logic)
                ListIterator<Sprite> spriteIter = objects.listIterator();
                ObjectsIteration: while ( spriteIter.hasNext() )
                {
                    Sprite e = spriteIter.next();

                    for (Sprite x : toRemove) {
                        if (e == x) {
                            spriteIter.remove();
                            toRemove.remove(x);
                            if (e instanceof Spaceship) {
                                score.value -= 100;
                                if (Spaceship.canRespawn()) {
                                    Spaceship.loseLife();
                                    Spaceship.respawnTimer++;
                                    System.out.printf("%d", Spaceship.lifeCounter);
                                }
                            }
                            if (e instanceof Shot) {
                                score.value += 10;
                            }
                            continue ObjectsIteration;
                        }
                    }
                    e.update(elapsedTime);

                    if (e instanceof Asteroid)
                    {
                        Asteroid a = (Asteroid) e;
                        toRemove.add(a.isHit(objects));

                        if (a.getHits() == 0)
                        {
                            spriteIter.remove();
                            Asteroid.remove();
                            continue;
                        }

                        while (Asteroid.getNum() < 15)
                        {
                            int f = (int)(Math.random() * 3) + 1;
                            spriteIter.add(new Asteroid(PLAYABLE_X, PLAYABLE_Y, f));
                        }
                    } else if (e instanceof Spaceship) {
                        if (keyInput.contains("LEFT"))
                            e.setAngle(e.angle - 3);
                        if (keyInput.contains("RIGHT"))
                            e.setAngle(e.angle + 3);
                        if (keyInput.contains("UP"))
                            e.addVelocityAtAngle(2);
                        if (keyInput.contains("SPACE")) {
                            Shot shot = ((Spaceship) e).fire(currentNanoTime);
                            if (shot != null) {
                                spriteIter.add(shot);
                            }
                        }

                        if (((Spaceship) e).hitBorder(PLAYABLE_X, PLAYABLE_Y, 0, 0)) {
                            e.setPosition(e.flipOverBorder(PLAYABLE_X,PLAYABLE_Y,0,0));
                        }
                    }

                    else if (e instanceof Shot) {
                        ((Shot)e).shotTimer++;
                        if (e.hitBorder(PLAYABLE_X, PLAYABLE_Y, 0, 0)) {
                            spriteIter.add(new Shot((e.flipOverBorder(PLAYABLE_X,PLAYABLE_Y,0,0)),e.angle,((Shot)e).shotTimer));
                            toRemove.add(e);
                        }
                         if (((Shot)e).shotTimer >= 120) {
                            toRemove.add(e);
                        }
                    }

                    //to delay respawn
                    if(Spaceship.respawnTimer > 0){
                        Spaceship.respawnTimer++;
                        if ((Spaceship.respawnTimer >= Spaceship.RESPAWN_DELAY)){
                            Spaceship.respawnTimer = 0;
                            spriteIter.add(new Spaceship(PLAYABLE_X / 2, PLAYABLE_Y / 2));
                        }
                    }
                    e.render(gc);
                    primaryStage.setTitle(String.format("Asteroids! Score: %s", score.toString()));

                }
            }
        }.start();


        primaryStage.show();
    }


    public static void main(String[] args) {
        launch(args);
    }
}